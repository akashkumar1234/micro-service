JFLAGS = -g
JC = c++c
.SUFFIXES: .cpp .class
.cpp.class:
        $(JC) $(JFLAGS) $*.cpp



default: classes

classes: $(CLASSES:.cpp=.class)

clean:
        $(RM) *.class
